const { Discover } = require('./lib/discover');
const { Device } = require('./lib/device');
const { parsePacket } = require('./lib/helpers');

const myDevice = {
    model: 'TX-NR696',
    address: '10.0.0.146',
    port: '60128',
    area: 'European or Asian model',
    uid: '0009B0F24F99'
};

const initDevice = info => {
    const device = new Device(info);
    device.on('data', data => {
        if (data.timeElapsed) return
        console.log('device onData:', data);
    });
    setTimeout(() => {
        device.sendMessage('LTN01');
        device.sendMessage('LTNQSTN');
    }, 2000);
};

initDevice(myDevice);

// const discover = new Discover();
//
// discover.on('device', device => {
//     // console.log('Found device', device);
//     initDevice(device);
// });
