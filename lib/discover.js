const { EventEmitter } = require('events');
const debug = require('debug')('eiscpDiscover');
const dgram = require('dgram');
const { parsePacket, createEiscpBuffer } = require('./helpers');
const { areas } = require('./constants');

const DEFAULT_PORT = 60128;
const DEFAULT_ADDRESS = '255.255.255.255';

class Discover extends EventEmitter {
    constructor(props = {}) {
        super(props);
        this.address = props.address || DEFAULT_ADDRESS;
        this.port = props.port || DEFAULT_PORT;
        this.devices = {};
        this.socket = null;
        this.socketInterval = false;
        this.onMessage = this.onMessage.bind(this);
        this.sendDiscoveryMessage = this.sendDiscoveryMessage.bind(this);
        Object.values(this.devices).forEach(device => this.emit('device', device));
        this.createServer();
    }

    createServer() {
        debug('Creating socket');
        clearInterval(this.socketInterval);
        this.socket = dgram.createSocket('udp4');

        this.socket.on('error', err => {
            debug('Socket onError:', err);
            this.socket.close();
            this.clearInterval();
            setTimeout(() => {
                this.createServer();
            }, 2000);
        });

        this.socket.on('message', (payload, info) => {
            const msg = parsePacket(payload);
            this.onMessage(msg, info);
        });

        this.socket.on('listening', () => {
            this.socket.setBroadcast(true);
            this.clearInterval();
            this.socketInterval = setInterval(this.sendDiscoveryMessage, 5000);
            this.sendDiscoveryMessage();
        });

        this.socket.bind(this.port);
    }

    sendDiscoveryMessage() {
        const buf = createEiscpBuffer('!xECNQSTN');
        this.socket.send(buf, 0, buf.length, this.port, this.address);
    }

    clearInterval() {
        clearInterval(this.socketInterval);
    }

    onMessage(msg, { address }) {
        debug(`Socket onMessage from ${address}:`, msg);

        if (msg === 'ECNQS') {
            // silently ignore ECNQS
        } else if (msg && msg.startsWith('ECN')) {
            try {
                const segments = msg.slice(3).split('/');
                const [model, port, areaCode, uid] = segments;

                if (uid) {
                    const result = {
                        model,
                        address,
                        port,
                        area: areas[areaCode],
                        uid: uid.slice(0, 12),
                    };

                    if (!this.devices[result.uid]) {
                        this.devices[result.uid] = result;
                        this.emit('device', result);
                    }

                }
            } catch (err) {
                debug(`Failed to parse discovery response: ${msg}`, err);
            }
        } else {
            debug('Unknown response:', msg);
        }
    }
}

module.exports = {
    Discover
};
