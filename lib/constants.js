const areas = {
    DX: 'North American model',
    XX: 'European or Asian model',
    JJ: 'Japanese model'
};

module.exports = {
    areas,
};
