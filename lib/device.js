const { EventEmitter } = require('events');
const net = require('net');
const debug = require('debug')('eiscpDevice');
const { parsePacket, createEiscpBuffer } = require('./helpers');
const parsers = require('./parsers/index');

class Device extends EventEmitter {
    constructor(props) {
        debug('Initializing device', props);
        super(props);
        this.address = props.address;
        this.port = props.port;
        this.uid = props.uid;
        this.model = props.model;
        this.connect({
            address: this.address,
            port: this.port,
        });
    }

    connect({ address, port }) {
        this.client = net.connect({ host: address, port });

        this.client.on('error', err => {
            console.log('client onError', err);
        });

        this.client.on('data', data => {

            const messages = parsePacket(data);
            debug('onData', messages);

            messages.forEach(({ code, message }) => {
                const result = this.parseMessage({ code, message });

                if (result === null) {
                    debug('Command not supported yet', { code, message });

                } else if (result) {
                    this.emit('data', result);

                } else {
                    console.log('!!!!!!!!!!!!!!', { code, message, result });
                }
            });
        });
    }

    parseMessage({ code, message }) {
        if (typeof parsers[code] === 'function') {
            return parsers[code]({ str: message });
        }
    }

    sendMessage(str) {
        if (!str.startsWith('!1')) {
            str = `!1${str}`;
        }
        const buf = createEiscpBuffer(str);
        this.client.write(buf);
    }
}

module.exports = {
    Device
};
