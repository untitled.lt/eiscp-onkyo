const { commands } = require('../../eiscp-commands-formated');

// System power
exports.PWR = ({ str }) => ({ power: str === '01' });

// Zone2 power
exports.ZPW = ({ str }) => ({ zone2Power: str === '01' });

// Zone3 power
exports.ZW3 = ({ str }) => ({ zone3Power: str === '01' });

// NJA2-http://10.0.0.146/album_art.cgi
exports.NJA = ({ str }) => {
    if (str.startsWith('2-http')) {
        return { meta: { coverArt: str.slice(2) } };
    } else {
        return { meta: { coverArt: '' } };
    }
};

// NTM00:01:02/00:02:46 NET/USB Time Info
exports.NTM = ({ str }) => {
    const [timeElapsed, timeTotal] = str.split('/');
    return { timeElapsed, timeTotal }
};

// NSTp-- NET/USB Play Status
exports.NST = ({ str }) => {
    /* NET/USB Play Status (3 letters)
     p -> Play Status: "S": STOP, "P": Play, "p": Pause, "F": FF, "R": FR
     r -> Repeat Status: "-": Off, "R": All, "F": Folder, "1": Repeat 1,
     s -> Shuffle Status: "-": Off, "S": All , "A": Album, "F": Folder
     */
    const [play, repeat, shuffle] = str;
    const playState = {
        S: 'stopped',
        P: 'playing',
        p: 'paused',
        F: 'fast forwarding',
        R: 'fast rewinding'
    };
    const repeatState = {
        '-': 'off',
        R: 'all',
        F: 'folder',
        1: 'repeat 1',
        x: 'unknown',
    };
    const shuffleState = {
        '-': 'off',
        S: 'all',
        A: 'album',
        F: 'folder',
        x: 'unknown',
    };
    return {
        isStopped: play === 'S',
        isPlaying: play !== 'S',
        isPaused: play === 's',
        isFF: play === 'F',
        isFR: play === 'R',
        isRepeatOn: repeat !== '-',
        isShuffleOn: shuffle !== '-',
        playStatus: playState[play],
        repeatStatus: repeatState[repeat],
        shuffleStatus: shuffleState[shuffle],
    };
};

// NTIJardin d'hiver NET/USB Title Name
exports.NTI = ({ str }) => ({ meta: { title: str } });

// NALLa Biographie de Luka Philipsen NET/USB Album Name Info
exports.NAL = ({ str }) => ({ meta: { album: str } });

// NATKeren Ann NET/USB Artist Name Info
exports.NAT = ({ str }) => ({ meta: { artist: str } });

// NLT0A22000000000001000A00 NET/USB List Title Info(for Network Control Only)
exports.NLT = ({ str }) => {
    /*
    xx : Service Type
    00 : DLNA, 01 : Favorite, 02 : vTuner, 03 : SiriusXM, 04 : Pandora, 05 : Rhapsody, 06 : Last.fm,
        07 : Napster, 08 : Slacker, 09 : Mediafly, 0A : Spotify, 0B : AUPEO!, 0C : radiko, 0D : e-onkyo,
        0E : TuneIn Radio, 0F : MP3tunes, 10 : Simfy, 11:Home Media, 12:Deezer, 13:iHeartRadio,
        F0 : USB Front, F1 : USB Rear, F2 : Internet Radio, F3 : NET, FF : None

    u : UI Type
    0 : List, 1 : Menu, 2 : Playback, 3 : Popup, 4 : Keyboard, 5 : Menu List

    y : Layer Info
    0 : NET TOP, 1 : Service Top,DLNA/USB/iPod Top, 2 : under 2nd Layer

    cccc : Current Cursor Position (HEX 4 letters)

    iiii : Number of List Items (HEX 4 letters)

    ll : Number of Layer(HEX 2 letters)

    rr : Reserved (2 leters)

    aa : Icon on Left of Title Bar
    00 : Internet Radio, 01 : Server, 02 : USB, 03 : iPod, 04 : DLNA, 05 : WiFi, 06 : Favorite
    10 : Account(Spotify), 11 : Album(Spotify), 12 : Playlist(Spotify), 13 : Playlist-C(Spotify)
    14 : Starred(Spotify), 15 : What's New(Spotify), 16 : Track(Spotify), 17 : Artist(Spotify)
    18 : Play(Spotify), 19 : Search(Spotify), 1A : Folder(Spotify)
    FF : None

    bb : Icon on Right of Title Bar
    00 : DLNA, 01 : Favorite, 02 : vTuner, 03 : SiriusXM, 04 : Pandora, 05 : Rhapsody, 06 : Last.fm,
        07 : Napster, 08 : Slacker, 09 : Mediafly, 0A : Spotify, 0B : AUPEO!, 0C : radiko, 0D : e-onkyo,
        0E : TuneIn Radio, 0F : MP3tunes, 10 : Simfy, 11:Home Media, 12:Deezer, 13:iHeartRadio,
        FF : None

    ss : Status Info
    00 : None, 01 : Connecting, 02 : Acquiring License, 03 : Buffering
    04 : Cannot Play, 05 : Searching, 06 : Profile update, 07 : Operation disabled
    08 : Server Start-up, 09 : Song rated as Favorite, 0A : Song banned from station,
        0B : Authentication Failed, 0C : Spotify Paused(max 1 device), 0D : Track Not Available, 0E : Cannot Skip
    nnn...nnn : Character of Title Bar (variable-length, 64 Unicode letters [UTF-8 encoded] max)"
    */
    const serviceTypes = {
        '00': 'Music Server (DLNA)',
        '01': 'Favorite',
        '02': 'vTuner',
        '03': 'SiriusXM',
        '04': 'Pandora',
        '05': 'Rhapsody',
        '06': 'Last.fm',
        '07': 'Napster',
        '08': 'Slacker',
        '09': 'Mediafly',
        '0A': 'Spotify',
        '0B': 'AUPEO!',
        '0C': 'radiko',
        '0D': 'e-onkyo',
        '0E': 'TuneIn Radio',
        '0F': 'MP3tunes',
        10: 'Simfy',
        11: 'Home Media',
        12: 'Deezer',
        13: 'iHeartRadio',
        18: 'Airplay',
        19: 'TIDAL',
        '1A': 'onkyo music',
        F0: 'USB/USB (Front)',
        F1: 'USB (Rear)',
        F2: 'Internet Radio',
        F3: 'NET',
        FF: 'None'
    };
    const statusInfos = {
        '00': 'None',
        '01': 'Connecting',
        '02': 'Acquiring License',
        '03': 'Buffering',
        '04': 'Cannot Play',
        '05': 'Searching',
        '06': 'Profile update',
        '07': 'Operation disabled',
        '08': 'Server Start-up',
        '09': 'Song rated as Favorite',
        '0A': 'Song banned from station',
        '0B': 'Authentication Failed',
        '0C': 'Spotify Paused(max 1 device)',
        '0D': 'Track Not Available',
        '0E': 'Cannot Skip'
    };

    const serviceTypeCode = str.slice(0, 2);
    const uiTypeCode = str.slice(2, 1);
    const layerInfoCode = str.slice(3, 1);
    const cursorPositionCode = str.slice(4, 4);
    const listItemsNumberCode = str.slice(8, 4);
    const reservedCode = str.slice(12, 2);
    const iconOnLeftCode = str.slice(14, 2);
    const iconOnRightCode = str.slice(16, 2);
    const statusInfoCode = str.slice(18, 2);
    const titleBar = str.slice(20);

    return {
        serviceType: serviceTypes[serviceTypeCode] || serviceTypeCode,
        statusInfo: statusInfos[statusInfoCode] || statusInfoCode,
        titleBar,
    };
};

// NTR----/---- NET/USB Track Info
exports.NTR = ({ str }) => {
    const [currentTrackNo, totalTrackNo] = str.split('/');
    return {
        currentTrackNo,
        totalTrackNo,
    };
};

// NDSE-x NET Connection/USB Device Status
exports.NDS = ({ str }) => {
    const [n, f, r] = str;
    const networkTypes = {
        '-': 'no connection',
        E: 'ethernet',
        W: 'wireless',
    };
    const USBTypes = {
        M: 'Memory/NAS',
        W: 'Wireless Adapter',
        B: 'Bluetooth Adapter',
        x: 'disabled',
    };
    return {
        networkType: networkTypes[n] || n,
        frontUSBType: USBTypes[f] || f,
        rearUSBType: USBTypes[r] || r,
    };
};

// SLI10 -- Input selector
exports.SLI = ({ str }) => {
    const mainInput = commands.main.SLI.values[str];
    if (mainInput) {
        return {
            mainInput: {
                code: str,
                names: mainInput.name,
            }
        };
    }
};

// MOT Music Optimizer / Sound Retriever
exports.MOT = ({ str }) => ({ musicOptimizer: str === '01' });

// MVL15 Master Volume
exports.MVL = ({ str }) => ({ masterVolume: parseInt(str, 16) });

// ZVL28 Zone2 Volume Command
exports.ZVL = ({ str }) => ({ zone2Volume: parseInt(str, 16) });

// RAS Cinema Filter
exports.RAS = ({ str }) => ({ cinemaFilter: str === '01' });

// AMT01 Audio Muting
exports.AMT = ({ str }) => ({ isMuted: str === '01' });

// ZMT Audio Zone2 Muting
exports.ZMT = ({ str }) => ({ isZone2Muted: str === '01' });

// MT3 Audio Zone2 Muting
exports.MT3 = ({ str }) => ({ isZone3Muted: str === '01' });

// AEQ00
exports.AEQ = ({ str }) => {
    const options = {
        '00': false,
        '01': 'all',
        '02': 'ex. Frontr L/R',
    };
    return { accuEQ: options[str] };
};

// LTN00
exports.LTN = ({ str }) => {
    const options = {
        '00': false,
        '01': 'low',
        '02': 'high',
        '03': 'auto',
    };
    return { lateNight: options[str] };
};


// LMD01 Listening Mode
exports.LMD = ({ str}) => {
    const mode = commands.main.LMD.values[str];
    if (mode) {
        return {
            listeningMode: {
                code: str,
                name: mode.name,
            }
        };
    }
};

// TRFB+9T+2
exports.TFR = ({ str }) => {
    const bassNegative = str.slice(1, 2) === '-';
    const bassValue = parseInt(str.slice(2, 3), 16);
    const trebleNegative = str.slice(4, 5) === '-';
    const trebleValue = parseInt(str.slice(5, 6), 16);

    return {
        bass: bassValue * (bassNegative ? -1 : 1),
        treble: trebleValue * (trebleNegative ? -1 : 1),
    };
};

// DIM
exports.DIM = ({ str }) => {
    const options = {
        '00': 'bright',
        '01': 'dim',
        '02': 'dark',
    };
    return { dimmerLevel: options[str] || str };
};

// HDO HDMI Output Selector
exports.HDO = ({ str }) => {
    const output = commands.main.HDO.values[str];
    if (output) {
        return {
            hdmiOutput: {
                code: str,
                names: output.name,
            }
        };
    }
};

// TODO: support PCT
// PCT
exports.PCT = ({ str }) => {
    console.log('phase controll', str);
};

// TODO: support NPB
// NPB12211000 NET/USB Playback view Button
exports.NPB = ({ str }) => null;

// TODO: support NMS
// NMSxxxxxx10a NET/USB Menu Status
exports.NMS = ({ str }) => null;

// TODO: support NDN
// NDN
exports.NDN = ({ str }) => null;

// TODO: support NLS
// NLS
exports.NLS = ({ str }) => null;

// TODO: support UPD
// UPD
exports.UPD = ({ str }) => null;






// VOC
// TODO: unknown command VOC
exports.VOC = ({ str }) => {
    return null;
};
