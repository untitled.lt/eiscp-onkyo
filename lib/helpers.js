const debug = require('debug')('eiscpHelpers');
const error = require('debug')('eiscp:error');

const PACKET_START = 'ISCP';

const createEiscpBuffer = str => {
    const iscpMsg = Buffer.from(`${str}\x0d`);
    const header = Buffer.from([
        73, 83, 67, 80, // magic
        0, 0, 0, 0, // header size
        0, 0, 0, 0, // data size
        1, // version
        0, 0, 0 // reserved
    ]);
    // write header size
    header.writeUInt32BE(16, 4);
    // write data size to eISCP header
    header.writeUInt32BE(iscpMsg.length, 8);
    return Buffer.concat([header, iscpMsg]);
};

const extractData = str => {
    const buf = Buffer.from(str, 'ascii');
    const start = buf.toString('ascii', 0, 4);
    const headerSize = buf.readIntBE(4, 4);
    const dataSize = buf.readIntBE(8, 4);
    let data;

    if (start.toUpperCase() !== PACKET_START || headerSize < 16) {
        error('Malformed message', buf.toString('ascii'));
        return;
    }

    if (dataSize > 0) {
        data = buf.toString('ascii', headerSize, headerSize + dataSize);
    }
    const startChar = data.slice(0, 1);
    const destination = data.slice(1, 2);
    const code = data.slice(2, 5);
    const message = data.slice(5, dataSize - 3);
    const full = buf.toString('ascii', 0, headerSize + dataSize);
    const rest = buf.slice(headerSize + dataSize);

    if (rest.length) {
        console.log('TOO MUCH DATA!!!');
        console.log({
            full,
            start,
            headerSize,
            dataSize,
            totalSize: buf.length,
            data,
            startChar,
            destination,
            code,
            message,
            rest: rest.toString('ascii')
        });
    }

    return {
        code,
        message,
    };
};

const parsePacket = buf1 => {
    const str = buf1.toString('ascii');
    const parts = str.split(PACKET_START)
        .filter(Boolean)
        .map(item => `${PACKET_START}${item}`);

    return parts.map(extractData);
};

module.exports = {
    parsePacket,
    createEiscpBuffer,
};
